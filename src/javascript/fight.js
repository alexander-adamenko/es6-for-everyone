export function fight(firstFighter, secondFighter) {
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;

  while(firstFighterHealth > 0 && secondFighterHealth > 0){
    secondFighterHealth -= getDamage(firstFighter, secondFighter);
    firstFighterHealth -= getDamage(secondFighter, firstFighter);
  }
  
  return firstFighterHealth > 0 ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
  let damage = getHitPower(attacker) - getBlockPower(enemy);
  
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  let criticalHitChance = Math.random() + 1;
  let hitPower = fighter.attack * criticalHitChance;

  return hitPower;
}

export function getBlockPower(fighter) {
  let dodgeChance = Math.random() + 1;
  let blockPower = fighter.defense * dodgeChance;

  return blockPower;
}
