import * as modal from '../modals/modal';
import { createElement } from '../helpers/domHelper';

export  function showWinnerModal(fighter) {

    const title = 'Winner';
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

    const { name } = fighter;
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
    nameElement.innerText = name;
    fighterDetails.append(nameElement);
    
    const { source } = fighter;
    const attributes = { src: source };
    const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
    fighterDetails.append(imgElement);

    const bodyElement = fighterDetails;
    modal.showModal({ title, bodyElement });

}