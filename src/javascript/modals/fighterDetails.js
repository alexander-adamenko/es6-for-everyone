import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name } = fighter;
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

 
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  nameElement.innerText = "Name: " + name;
  fighterDetails.append(nameElement);
  fighterDetails.append(createElement({ tagName: 'br', className: 'delimiter' }));
  

  const { attack } = fighter;
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  attackElement.innerText = "Attack: " + attack;
  fighterDetails.append(attackElement);
  fighterDetails.append(createElement({ tagName: 'br', className: 'delimiter' }));

  const { defense } = fighter;
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  defenseElement.innerText = "Defense: " + defense;
  fighterDetails.append(defenseElement);
  fighterDetails.append(createElement({ tagName: 'br', className: 'delimiter' }));

  const { health } = fighter;
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  healthElement.innerText = "Health: " + health;
  fighterDetails.append(healthElement);
  fighterDetails.append(createElement({ tagName: 'br', className: 'delimiter' }));

  const { source } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  fighterDetails.append(imgElement);
  
  return fighterDetails;
}